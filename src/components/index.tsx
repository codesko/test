import * as React from 'react';
import Menu from './menu';

const App: React.FC = () => {
    return (
        <React.Fragment>
            <Menu />
        </React.Fragment>
    );
};

export default App;
