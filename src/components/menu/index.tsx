import * as React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faBars,
    faTimes
} from '@fortawesome/free-solid-svg-icons';
import { HamburgerMenu, HamburgerMenuExitIconWrapper, HamburgerMenuIconWrapper } from './style';
import { useKeyboardEvent } from '../../hooks/useKeyboardEvent';
import { MenuOverlayWithTransition } from './overlay';

const Menu: React.FC = () => {
    const [visible, setVisible] = React.useState(false);

    useKeyboardEvent('Escape', () => {
        setVisible(false);
    });

    return (
        <>
            <HamburgerMenu onClick={() => setVisible(!visible)}>
                <HamburgerMenuIconWrapper isVisible={visible}>
                    <FontAwesomeIcon
                        icon={faBars}
                        size="2x"
                    />
                </HamburgerMenuIconWrapper>
                <HamburgerMenuExitIconWrapper isVisible={visible}>
                    <FontAwesomeIcon
                        icon={faTimes}
                        color="white"
                        size="2x"
                    />
                </HamburgerMenuExitIconWrapper>
            </HamburgerMenu>
            <MenuOverlayWithTransition isVisible={visible} />
        </>
    );
};

export default Menu;