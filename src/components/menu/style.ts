import styled from 'styled-components';

export interface MenuProps {
    isVisible: boolean;
}

export const HamburgerMenu = styled.div`
    position: fixed;
    right: 1em;
    top: 2em;
    z-index: 2;
`;

export const HamburgerMenuIconWrapper = styled.span`
    ${(props: MenuProps) => props.isVisible && `
        display: none
    `}
`;

export const HamburgerMenuExitIconWrapper = styled.span`
    display: none;
    
    ${(props: MenuProps) => props.isVisible && `
        display: block;
    `}
`;