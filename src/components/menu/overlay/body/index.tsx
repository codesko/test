import * as React from 'react';
import {
    MenuBody,
    MenuListDesktop,
    MenuListItem,
    MenuListItemLink,
    MenuListMobile,
    MenuUser
} from './style';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { menu, mobileMenu, sideMenu } from './data';
import { MenuUserInfo } from './userInfo';

interface MenuListInterface {
    name: string;
    location: string;
    rotation?: any;
    icon?: any;
    margin?: string;
}

export const MenuOverlayBody: React.FC = () => {
    return (
        <MenuBody>
            <MenuListDesktop>
                { sideMenu.map((e: MenuListInterface, i: number) =>
                    <MenuListItem key={'menuList' + i} margin={e.margin !== undefined ? e.margin : ''}>
                        <MenuListItemLink href={e.location}>
                            { e.name }
                        </MenuListItemLink>
                    </MenuListItem>
                )}
            </MenuListDesktop>
            <MenuUser>
                <MenuUserInfo />
                <MenuListDesktop>
                    { menu.map((e: MenuListInterface, i: number) =>
                        <MenuListItem key={'menuList' + i}>
                            <MenuListItemLink href={e.location}>
                                { e.name }
                            </MenuListItemLink>
                        </MenuListItem>
                    )}
                </MenuListDesktop>
                <MenuListMobile>
                    { mobileMenu.map((e: MenuListInterface, i: number) =>
                        <MenuListItem key={'menuListMobile' + i}>
                            <MenuListItemLink href={e.location}>
                                <FontAwesomeIcon
                                    icon={e.icon}
                                    rotation={e.rotation || undefined}
                                />
                                <span>{ e.name }</span>
                            </MenuListItemLink>
                        </MenuListItem>
                    )}
                </MenuListMobile>
            </MenuUser>
        </MenuBody>
    );
};