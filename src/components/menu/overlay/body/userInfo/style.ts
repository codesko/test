import styled from 'styled-components';
import { media } from '../../../../../helpers/media';
import { theme } from '../../../../../helpers/theme';

export const MenuUserHeader = styled.header`
    border-bottom: 1px solid ${theme.colors.borderThin};
    padding: 15px 0;
    display: flex;
    align-items: center;

    ${media.tablet`
        padding: 15px 20px;
        justify-content: center;
    `}

    ${media.phone`
        padding: 15px 0 0 0;
        justify-content: center;
        flex-direction: column;
        border-bottom: none;
    `}
`;

export const MenuUserHeaderAvatar = styled.img`
    width: 50px;
    height: 50px;

    ${media.phone`
        width: 32px;
        height: 32px;
    `}
`;

export const MenuUserHeaderAvatarWrapper = styled.div`
    background-clip: content-box;
    display: flex;
    align-items: center;
    justify-content: center;
    min-width: 77px;
    min-height: 77px;
    border-radius: 50%;
    border: 2px solid white;
    padding: 2px;
    background-color: white;

    ${media.phone`
        background-clip: inherit;
        border: none;
        min-width: 45px;
        min-height: 45px;
        margin-bottom: .5em;
    `}
`;

export const MenuUserHeaderDescription = styled.div`
    margin-left: 20px;
    
    p {
        margin: 0;
        color: white;
        font-size: 1.5rem;
        font-weight: 700;

        &:first-of-type {
            font-weight: 900;
        }
    }

    ${media.phone`
        margin: 0;
        text-align: center;
        
        p {
            font-size: .8em;
            font-weight: 500;
            line-height: 1.3em;
            
            &:first-of-type {
                font-size: 1em;
            }
            
            &:last-of-type {
                font-size: .9em;
            }
        }
    `}
`;