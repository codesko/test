interface UserInterface {
    name: string;
    funds: number;
    region: string;
    currency: string;
}

export const user: UserInterface = {
    name: 'Hulk',
    currency: 'GBP',
    region: 'en-GB',
    funds: 1234.56
};

export const priceFormatter = new Intl.NumberFormat(
    user.region,
    {
        style: 'currency',
        currency: user.currency
    }
);