import * as React from 'react';
import {
    MenuUserHeader,
    MenuUserHeaderAvatar,
    MenuUserHeaderAvatarWrapper,
    MenuUserHeaderDescription
} from './style';

import * as Avatar from './images/avatar.png';

import { priceFormatter, user } from './data';
import { Mobile, Desktop } from '../../../../../helpers/media';

export const MenuUserInfo: React.FC = () => {
    return (
        <MenuUserHeader>
            <Desktop>
                <MenuUserHeaderAvatarWrapper>
                    <MenuUserHeaderAvatar src={Avatar}/>
                </MenuUserHeaderAvatarWrapper>
                <MenuUserHeaderDescription>
                    <p>{ user.name }</p>
                    <p>{ priceFormatter.format(user.funds) } Available</p>
                </MenuUserHeaderDescription>
            </Desktop>
            <Mobile>
                <MenuUserHeaderAvatarWrapper>
                    <MenuUserHeaderAvatar src={Avatar}/>
                </MenuUserHeaderAvatarWrapper>
                <MenuUserHeaderDescription>
                    <p>{ user.name }</p>
                    <p>Available funds</p>
                    <p>{ priceFormatter.format(user.funds) }</p>
                </MenuUserHeaderDescription>
            </Mobile>
        </MenuUserHeader>
    );
};