import {
    faUserCircle,
    faPlane,
    faCreditCard,
    faSignOutAlt,
    faQuestionCircle,
    faLifeRing,
    faPhone,
    faInfoCircle,
} from '@fortawesome/free-solid-svg-icons';

export const sideMenu = [
    {
        name: 'Home',
        location: '/'
    },
    {
        name: 'Flights',
        location: '/'
    },
    {
        name: 'About Us',
        location: '/',
        margin: '10px 0 0 0'
    },
    {
        name: 'FAQ',
        location: '/'
    },
    {
        name: 'Support',
        location: '/'
    },
    {
        name: 'Contact Us',
        location: '/'
    },
];

export const menu = [
    {
        name: 'Profile',
        location: '/'
    },
    {
        name: 'My Bookings',
        location: '/'
    },
    {
        name: 'My Payments',
        location: '/'
    },
    {
        name: 'Log Out',
        location: '/'
    },
    {
        name: 'Resume Application',
        location: '/'
    }
];

export const mobileMenu = [
    {
        name: 'Profile',
        icon: faUserCircle,
        location: '/'
    },
    {
        name: 'My Bookings',
        icon: faPlane,
        rotation: 270,
        location: '/'
    },
    {
        name: 'My Payments',
        icon: faCreditCard,
        location: '/'
    },
    {
        name: 'Support',
        icon: faLifeRing,
        location: '/'
    },
    {
        name: 'Contact Us',
        icon: faPhone,
        rotation: 90,
        location: '/'
    },
    {
        name: 'Log Out',
        icon: faSignOutAlt,
        location: '/'
    },
    {
        name: 'About',
        icon: faQuestionCircle,
        location: '/'
    },
    {
        name: 'FAQ',
        icon: faInfoCircle,
        location: '/'
    }
];