import styled from 'styled-components';
import { media } from '../../../../helpers/media';

interface MenuListItemProps {
    margin?: string;
}

export const MenuBody = styled.div`
    justify-items: center;
    display: grid;
    margin: 15px 0;
    grid-gap: 15px;
    grid-template-columns: 1fr 1.5fr 1fr;
    grid-template-areas: "list user .";

    ${media.tablet`
        grid-template-columns: inherit;
        grid-template-areas:
            "list"
            "user"
    `}

    ${media.phone`
        display: flex;
    `}
`;

export const MenuListDesktop = styled.ul`
    grid-area: list;
    justify-self: left;
    padding: 0;
    list-style: none;
    
    ${media.phone`
        display: none;
    `}
`;

export const MenuListMobile = styled.ul`
    display: none;
    grid-area: mobile-list;
    justify-self: left;
    padding: 0;
    list-style: none;

    ${media.phone`
        display: inherit;
        line-height: 3rem;
    `}
`;

export const MenuUser = styled.div`
    grid-area: user;

    ${media.tablet`
        width: 100%;
        border: 1px solid #f18990;
        border-radius: 10px;

        ul {
            padding: 0 20px;
        }
    `}

    ${media.phone`
        width: 100%;
        border: 0;
        border-radius: 0;

        ul {
            padding: 0;
        }
    `}
`;

export const MenuListItem = styled.li`    
    ${(props: MenuListItemProps) => props.margin && `
        margin: ${props.margin}
    `}

    ${media.phone`
        line-height: 3rem;
        border-bottom: 1px solid #f18990;
        
        &:last-child {
            border-bottom: 0;
        }
    `}
`;

export const MenuListItemLink = styled.a`
    color: white;
    font-size: 2rem;
    font-weight: 700;
    text-decoration: none;
    
    :hover {
        text-decoration: underline;
    }
    
    ${media.phone`
        font-size: 1rem;
        font-weight: 500;
        
        span {
            margin-left: .7rem
        }
    `}
`;