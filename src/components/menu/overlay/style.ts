import styled from 'styled-components';
import { media } from '../../../helpers/media';
import { theme } from '../../../helpers/theme';

export const MenuStyled = styled.div`
    min-height: 100vh;
    width: 100vw;
    padding: 0 16px;
    background: ${theme.colors.red};
    display: grid;
    grid-template-columns: 1fr auto;
    grid-template-rows: max-content min-content min-content max-content;
    grid-template-areas: 
        "header header header" 
        "list user ." 
        "footer footer footer";

    ${media.tablet`
        grid-template-columns: auto;
        grid-template-areas: 
            "header" 
            "list" 
            "user" 
            "footer";
    `}

    ${media.phone`
        grid-template-columns: auto;
        grid-template-areas: 
            "header"
            "user"
            "footer";
    `}
`;