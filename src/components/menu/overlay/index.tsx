import * as React from 'react';
import { MenuOverlayHeader } from './header';
import { MenuOverlayFooter } from './footer';
import { MenuOverlayBody } from './body';
import { MenuStyled } from './style';
import { Transition } from 'react-transition-group';

interface MenuProps {
    isVisible: boolean;
}

const MenuOverlay: React.FC = () => {
    return (
        <MenuStyled>
            <MenuOverlayHeader />
            <MenuOverlayBody />
            <MenuOverlayFooter />
        </MenuStyled>
    );
};

export const MenuOverlayWithTransition: React.FC<MenuProps> = props => {
    const duration = 300;

    const defaultStyle = {
        transition: `opacity ${duration}ms ease-in-out`,
        opacity: 0,
        zIndex: -1
    };

    const transitionStyles = {
        entering: { opacity: 1 },
        entered: { opacity: 1, zIndex: 1 },
        exiting: { opacity: 0 },
        exited: { opacity: 0 },
    };

    return (
        <Transition
            in={props.isVisible}
            timeout={300}
        >
            {state => (
                <div style={{...defaultStyle, ...transitionStyles[state]}}>
                    <MenuOverlay />
                </div>
            )}
        </Transition>
    );
};