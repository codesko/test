import * as React from 'react';
import * as Logo from './images/awaymoFullWhite.svg';
import { MenuHeader, MenuHeaderLogo } from './style';

export const MenuOverlayHeader: React.FC = () => {
    return (
        <MenuHeader>
            <MenuHeaderLogo src={Logo}/>
        </MenuHeader>
    );
};