import styled from 'styled-components';
import { media } from '../../../../helpers/media';

export const MenuHeader = styled.header`
    grid-area: header;
    height: 100px;
    display: flex;
    align-items: center;
    border-bottom: 2px solid white;
    
    span {
        margin-left: auto
    }
    
    ${media.phone`
        border-bottom: 1px solid #f18990;
    `}
`;

export const MenuHeaderLogo = styled.img`
    width: 180px;
    height: 35px;

    ${media.phone`
        width: 134px;
        height: 26px;
    `}
`;