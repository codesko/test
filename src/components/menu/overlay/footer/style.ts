import styled from 'styled-components';
import { media } from '../../../../helpers/media';
import { theme } from '../../../../helpers/theme';

export const MenuFooter = styled.footer`
    grid-area: footer;
    padding: 20px 0;
    line-height: 2rem;
    border-top: 1px solid ${theme.colors.borderThin};
    text-align: center;

    ${media.tablet`
        border-top: 0;
    `}

    ${media.phone`
        border-top: 1px solid #f18990;
        line-height: 1.5rem;
    `}
`;

export const MenuFooterLink = styled.a`
    color: white;
    font-size: 1.5rem;
    font-weight: 500;
    display: block;
    text-decoration: none;
    
    ${media.phone`
        font-size: 1rem;
        font-weight: 500;
    `}
`;

export const MenuFooterLabel = styled.label`
    color: white;
    font-size: 1.5rem;
    font-weight: 500;
    
    ${media.phone`
        font-size: 1rem;
    `}
`;