import * as React from 'react';
import { MenuFooter, MenuFooterLabel, MenuFooterLink } from './style';
import { Desktop } from '../../../../helpers/media';

export const MenuOverlayFooter: React.FC = () => {
    const phone: string = `+44 (0) 20 8050 3459`;
    const email: string = `support@awaymo.com`;
    const label: string = `We're here to help`;

    return (
        <MenuFooter>
            <MenuFooterLabel>
                { label }
            </MenuFooterLabel>
            <Desktop>
                <MenuFooterLink
                    href={`callto:${phone}`}
                    target="_blank"
                >
                    { phone }
                </MenuFooterLink>
                <MenuFooterLink
                    href={`mailto:${email}`}
                    target="_blank"
                >
                    { email }
                </MenuFooterLink>
            </Desktop>
        </MenuFooter>
    );
};