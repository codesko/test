import * as React from 'react';

export const useKeyboardEvent = (key: string, callback: () => any) => {
    React.useEffect(
        () => {
            const handler = (event: any) => {
                if (event.key === key) {
                    callback();
                }
            };
            window.addEventListener('keydown', handler);

            return () => {
                window.removeEventListener('keydown', handler);
            };
        },
        []
    );
};
