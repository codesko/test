import * as React from 'react';
import { css } from 'styled-components';
import Responsive from 'react-responsive';

const breakpoints: any = {
    desktop: 992,
    tablet: 768,
    phone: 576,
};

export const Desktop: React.FC<any> = props => <Responsive {...props} minWidth={breakpoints.tablet} />;
export const Tablet: React.FC<any> = props => <Responsive {...props} minWidth={breakpoints.tablet} maxWidth={991} />;
export const Mobile: React.FC<any> = props => <Responsive {...props} maxWidth={breakpoints.tablet - 1} />;

export const media = Object.keys(breakpoints).reduce(
    (acc: any, label: string) => {
        acc[label] = (...args: any) => css`
            @media (max-width: ${breakpoints[label] / 16}em) {
                ${css(args)}
            }
        `;

        return acc;
    },
    {}
);